
function getRandomIntInclusive(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
}

function getRandomBool(): boolean {
    return ((getRandomIntInclusive(0, 1) == 0) ? false : true);
}

enum CarType {
    BUICK,
    MAZDA,
    FORD,
    TESLA,
    FERRARI,
    LAMBORGHINI,
    HONDA,
    HYUNDAI,
    JEEP,
    BMW,
}

interface Car {
    model: CarType;
    purchasePrice: number;
    lotPrice: number;
    isInspected: boolean;
}

class Car {
    private _model: CarType;
    private _purchasePrice: number;
    private _lotPrice: number;
    private _isInspected: boolean;
    private _profitValue: number;

    constructor(modelInput: CarType, purchasePriceInput: number = getRandomIntInclusive(69000, 420000), lotPriceInput: number = getRandomIntInclusive(69000, 420000), isInspectedInput: boolean = getRandomBool()) {
        if (modelInput > 9 || modelInput < 0) {
            this._model = getRandomIntInclusive(0, 9);
        } else {
            this._model = modelInput;
        }

        this._purchasePrice = purchasePriceInput;
        this._lotPrice = lotPriceInput;
        this._isInspected = isInspectedInput;
        this._profitValue = (this._lotPrice - this._purchasePrice);
    }

    getCarType(): CarType {
        return (this._model);
    }

    negativeInspection(): void {
        console.log('This ' + Object.values(CarType)[this._model] + ' has not been inspected.');
    }

    positiveInspection(): void {
        console.log('This ' + Object.values(CarType)[this._model] + ' has been inspected.');
    }

    inspectResult(): void {
        this._isInspected ? this.positiveInspection() : this.negativeInspection();
    }

    displayProfit(): void {
        console.log('The profit of this ' + Object.values(CarType)[this._model] + ' is $' + this._profitValue);
    }

    positiveInvest(): void {
        console.log('This ' + Object.values(CarType)[this._model] + ' is a good investment, you should purchase this.');
    }

    negativeInvest(): void {
        console.log('This ' + Object.values(CarType)[this._model] + ' is a bad investment, you should not purchase this.');
    }

    isProfit(): void {
        this._profitValue > 0 && this._isInspected == true ? this.positiveInvest() : this.negativeInvest();
    }

    carResultOutput(): void {
        this.inspectResult();
        this.displayProfit();
        this.isProfit();
    }
}

const carList: Car[] = [];

for (let index = 0; index < 100; index++) {
    carList[index] = new Car(10);
    console.log("Car at index point: " + index + " has been created");
    carList[index].carResultOutput();
}

// for (let index = 0; index < carArray.length; index++) {
//     carResultOutput(carArray[index]);
//     console.log('');
// };

// function fibo(goal: number) {
//     let num1: number = 0;
//     let num2: number = 1;
//     let temp: number;

//     while (num2 < goal) {
//         temp = num2 + num1;
//         num1 = num2;
//         num2 = temp;
//         console.log(num1);
//     }
// }

// fibo(999);